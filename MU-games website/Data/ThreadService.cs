using System;
using System.Data;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Text;
using MySql.Data.MySqlClient;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace MU_games_website.Data
{
    public class ThreadService:MySQLCRService<ThreadService>
    {
        private readonly IConfiguration _configuration;
        private MySqlCommand GetThreads= new MySqlCommand("SELECT * FROM multi_universe_games.threads_list");
        MySqlCommand InsertThread= new MySqlCommand
            (
                @"
                INSERT INTO
                    multi_universe_games.threads 
                    (thread_name,thread_description,creation_date,thread_author_id)
                VALUES 
                    (
                    @Tittle,
                    @Description,
                    CURRENT_DATE(),
                    (
                    SELECT
                        users.user_id
                    FROM 
                        multi_universe_games.users
                    WHERE
                        users.login = @Author)
                    );
                "
            );
        public int Id{get; set;}
        [Required (ErrorMessage="Введите название форума")]
        [MaxLength(100,ErrorMessage="Описание более 100 символов")]
        public string Name{get; set;}
        [Required (ErrorMessage="Введите описание форума")]
        [MaxLength(255,ErrorMessage="Описание более 255 символов")]
        public string Description{get; set;}
        public DateTime CreationDate{get; set;}
        public string Author{get; set;}
        public ThreadService(IConfiguration configuration):base(configuration)=>_configuration=configuration;
        public ThreadService(IConfiguration configuration, int Id, string Name, string Description, DateTime CreationDate, string Author)
        :base(configuration)
        {   _configuration=configuration;
            this.Id=Id;
            this.Name=Name;
            this.Description=Description;
            this.CreationDate=CreationDate;
            this.Author=Author;
        }
        protected override ObservableCollection<ThreadService> GetData(MySqlCommand comm)
        {
            var Data= GetDataTable(comm);
            var Result= new ObservableCollection<ThreadService>();
            for(int i=0; i!=Data.Rows.Count; i++)
            {
                Result.Add(
                    new ThreadService
                    (
                        _configuration,
                        (int)Data.Rows[i][0],
                        (string)Data.Rows[i][1],
                        (string)Data.Rows[i][2],
                        (DateTime)Data.Rows[i][3],
                        (string)Data.Rows[i][4]
                    )
                );
            }
            return Result;
        }

        protected override async Task<ObservableCollection<ThreadService>> GetDataAsync(MySqlCommand comm)
        {
            var Data=  await GetDataTableAsync(comm);
            var Result= new ObservableCollection<ThreadService>();
            for(int i=0; i!=Data.Rows.Count; i++)
            {
                Result.Add(
                    new ThreadService
                    (
                        _configuration,
                        (int)Data.Rows[i][0],
                        (string)Data.Rows[i][1],
                        (string)Data.Rows[i][2],
                        (DateTime)Data.Rows[i][3],
                        (string)Data.Rows[i][4]
                    )
                );
            }
            return Result;
        }
        public Task<ObservableCollection<ThreadService>> GetAllThreads()=>Task.FromResult(GetData(GetThreads));
        public async Task<ObservableCollection<ThreadService>> GetAllThreadsAsync()=> await GetDataAsync(GetThreads);
        public void AddThread()
        {
            InsertThread.Parameters.AddWithValue("@Tittle",Name);
            InsertThread.Parameters.AddWithValue("@Description",Description);
            InsertThread.Parameters.AddWithValue("@Author",Author);
            RunCommand(InsertThread);
            InsertThread.Parameters.Clear();
        }
        public void AddThreadAsync()
        {
            InsertThread.Parameters.AddWithValue("@Tittle",Name);
            InsertThread.Parameters.AddWithValue("@Description",Description);
            InsertThread.Parameters.AddWithValue("@Author",Author);
            RunCommandAsync(InsertThread);
            InsertThread.Parameters.Clear();
        }

    }
}