using System;
using System.Data;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Text;
using MySql.Data.MySqlClient;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace MU_games_website.Data
{
    public class MessagingService:MySQLCRService<MessagingService>
    {
        private MySqlCommand GetMessages=new MySqlCommand
        (
            @"
            SELECT 
                * 
            FROM 
                multi_universe_games.messages_list 
            WHERE 
                messages_list.thread_name = @Thread;
            "
        );
        private MySqlCommand InsertMessage= new MySqlCommand
        (
            @"
            INSERT INTO 
                multi_universe_games.thread_messages 
                (message,thread_id,user_id,message_date)
            VALUES 
            (
                @Message,
                (select distinct 
                    threads.idthread 
                FROM 
                    multi_universe_games.threads
                WHERE threads.thread_name = @Thread
                ),
                (SELECT
                    users.user_id
                FROM 
                multi_universe_games.users
                WHERE
                users.login =@User)
                ,
            current_timestamp
            );   
            "
        );
        private IConfiguration _configuration;
        public int MessageId{get; set;}
        [Required (ErrorMessage="Введите сообщение")]
        [MaxLength(255,ErrorMessage="Сообщение более 255 символов")]
        public string Message{get; set;}
        public string Thread{get; set;}
        public string Author{get; set;}
        public DateTime MessageDate {get; set;}
        public MessagingService(IConfiguration configuration):base(configuration)=>_configuration=configuration;
        public MessagingService(IConfiguration configuration, int MessageId, string Message, string Thread, string Author,DateTime MessageDate)
        :base(configuration)
        {
            _configuration=configuration;
            this.MessageId=MessageId;
            this.Message=Message;
            this.Thread=Thread;
            this.Author=Author;
            this.MessageDate=MessageDate;
        }
        protected override ObservableCollection<MessagingService> GetData(MySqlCommand comm)
        {
            var Result= new ObservableCollection<MessagingService>();
            var Data= GetDataTable(comm);
            for(int i=0; i!=Data.Rows.Count; i++)
            {
                Result.Add(
                    new MessagingService
                    (
                        _configuration,
                        (int)Data.Rows[i][0],
                        (string)Data.Rows[i][1],
                        (string)Data.Rows[i][2],
                        (string)Data.Rows[i][3],
                        (DateTime)Data.Rows[i][4]
                    )
                );
            }
            return Result;

        }
         protected override async Task<ObservableCollection<MessagingService>> GetDataAsync(MySqlCommand comm)
        {
            var Result= new ObservableCollection<MessagingService>();
            var Data= await GetDataTableAsync(comm);
            for(int i=0; i!=Data.Rows.Count; i++)
            {
                Result.Add(
                    new MessagingService
                    (
                        _configuration,
                        (int)Data.Rows[i][0],
                        (string)Data.Rows[i][1],
                        (string)Data.Rows[i][2],
                        (string)Data.Rows[i][3],
                        (DateTime)Data.Rows[i][4]
                    )
                );
            }
            return Result;
        }
        public Task<ObservableCollection<MessagingService>> GetThread()
        {
            GetMessages.Parameters.AddWithValue("@Thread",Thread);
            var Result=GetData(GetMessages);
            GetMessages.Parameters.Clear();
            return Task.FromResult(Result);
        }

        public async Task<ObservableCollection<MessagingService>> GetThreadAsync()
        {
            GetMessages.Parameters.AddWithValue("@Thread",Thread);
            var Result= await GetDataAsync(GetMessages);
            GetMessages.Parameters.Clear();
            return Result;
        }

        public void SendMessage()
        {
            InsertMessage.Parameters.AddWithValue("@Message", Message);
            InsertMessage.Parameters.AddWithValue("@Thread", Thread);
            InsertMessage.Parameters.AddWithValue("@User", Author);
            RunCommand(InsertMessage);
            InsertMessage.Parameters.Clear();
        }
         public void SendMessageAsync()
        {
            InsertMessage.Parameters.AddWithValue("@Message", Message);
            InsertMessage.Parameters.AddWithValue("@Thread", Thread);
            InsertMessage.Parameters.AddWithValue("@User", Author);
            RunCommandAsync(InsertMessage);
            InsertMessage.Parameters.Clear();
        }
    }
}