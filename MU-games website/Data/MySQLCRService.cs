﻿using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Threading.Tasks;

namespace MU_games_website.Data
{
    public abstract class MySQLCRService<T>

    {
        private MySqlConnection conn;
        private readonly IConfiguration _configuration;

        public MySQLCRService(IConfiguration configuration)
        {
            _configuration = configuration;
            conn = new MySqlConnection(_configuration.GetSection("ConnectionStrings").GetSection("SiteContent").Value);
        }

        protected DataTable GetDataTable(MySqlCommand comm)
        {
            DataTable datatable = new DataTable();
            conn.Open();
            if (conn.State == ConnectionState.Open)
            {
                try
                {
                    if (conn != null)
                    {
                        comm.Connection = conn;
                        datatable.Load(comm.ExecuteReader());
                        
                    }
                }
                catch (Exception E)
                {
                    Console.WriteLine("Подключение к базе данных " + E.Message + "\n" + "Статус подключения " + conn.State + "\n" + E.StackTrace);
                }
            }
            conn.Close();
            return datatable;
        }

        protected async Task<DataTable> GetDataTableAsync(MySqlCommand comm)
        {
            DataTable datatable = new DataTable();
            await conn.OpenAsync();
            if (conn.State == ConnectionState.Open)
            {
                try
                {
                    if (conn != null)
                    {
                        comm.Connection = conn;
                        datatable.Load(await comm.ExecuteReaderAsync());
                        
                    }
                }
                catch (Exception E)
                {
                    Console.WriteLine("Подключение к базе данных " + E.Message + "\n" + "Статус подключения " + conn.State + "\n" + E.StackTrace);
                }
            }
            await conn.CloseAsync();
            return datatable;
        }

        protected abstract ObservableCollection<T> GetData(MySqlCommand comm);
        protected abstract Task<ObservableCollection<T>> GetDataAsync(MySqlCommand comm);

        public void RunCommand(MySqlCommand comm)
        {
            conn.Open();
            if (conn.State == ConnectionState.Open)
            {
                try
                {
                    comm.Connection = conn;
                    comm.ExecuteNonQuery();
                    
                }
                catch (Exception E)
                {
                    Console.WriteLine("Подключение к базе данных " + E.Message + "\n" + "Статус подключения " + conn.State + "\n" + E.StackTrace);
                }
                conn.Close();
            }
        }

        public async void RunCommandAsync(MySqlCommand comm)
        {
            await conn.OpenAsync();
            if (conn.State == ConnectionState.Open)
            {
                try
                {
                    comm.Connection = conn;
                    await comm.ExecuteNonQueryAsync();
                    
                }
                catch (Exception E)
                {
                    Console.WriteLine("Подключение к базе данных " + E.Message + "\n" + "Статус подключения " + conn.State + "\n" + E.StackTrace);
                }
                await conn.CloseAsync();
            }
        }
    }
}
