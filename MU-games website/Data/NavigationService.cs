using System;
using System.Data;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace MU_games_website.Data
{
    public class NavigationService:MySQLCRService<NavigationService>
    {
        private readonly IConfiguration _configuration;
        private MySqlCommand NavLinksRequest= new MySqlCommand
        (
            @"
            SELECT
                * 
            FROM 
                multi_universe_games.access_links
             WHERE
                access_role = 'public'
            "
        );
        private MySqlCommand AuthorizedLinksRequest=new MySqlCommand
        (   
            @"
            SELECT DISTINCT
                *
            FROM 
                multi_universe_games.access_links
            WHERE
                access_role = 'user'  
                or access_role = 
                    (
                    SELECT
                        user_role
                    FROM 
                        multi_universe_games.accounts 
                    WHERE 
                        accounts.login=@login
                    );
            "
        );
        public int IdLink{ get; set;}
        [Required(ErrorMessage ="Введите адресс ссылки")]
        public string Adress{ get ;set;}
        [Required(ErrorMessage ="Введите имя ссылки")]
        public string Header{ get ;set;}
        [Required(ErrorMessage ="Назначьте уровень доступа")]
        public string AccessLevel{ get ;set;}
        public NavigationService(IConfiguration configuration):base(configuration)=>_configuration=configuration;
        public NavigationService(IConfiguration configuration,
        int IdLink, string Adress, string Header, string AccessLevel):base(configuration){
            _configuration=configuration;
            this.IdLink=IdLink;
            this.Adress=Adress;
            this.Header=Header;
            this.AccessLevel=AccessLevel;
        }
        protected override ObservableCollection<NavigationService> GetData(MySqlCommand comm)
        {
            var NavigationService= new ObservableCollection<NavigationService>();
            var NavigationTable= GetDataTable(comm);
            for(int i=0; i!=NavigationTable.Rows.Count; i++)
            {
                try
                {
                    NavigationService.Add
                    (
                        new NavigationService
                        (
                            _configuration,
                            (int)NavigationTable.Rows[i][0],
                            (string)NavigationTable.Rows[i][1],
                            (string)NavigationTable.Rows[i][2],
                            (string)NavigationTable.Rows[i][3]
                        )
                    );
                }
                catch(Exception ex)
                {
                    Console.WriteLine("Ошибка в получении навигационных ссылок \n"+ ex.Message+"\n"+ex.StackTrace+"\n");
                }
            }
            return NavigationService;
        }

         protected override async Task<ObservableCollection<NavigationService>> GetDataAsync(MySqlCommand comm)
        {
            var NavigationService= new ObservableCollection<NavigationService>();
            var NavigationTable= await GetDataTableAsync(comm);
            for(int i=0; i!=NavigationTable.Rows.Count; i++)
            {
                try
                {
                    NavigationService.Add
                    (
                        new NavigationService
                        (
                            _configuration,
                            (int)NavigationTable.Rows[i][0],
                            (string)NavigationTable.Rows[i][1],
                            (string)NavigationTable.Rows[i][2],
                            (string)NavigationTable.Rows[i][3]
                        )
                    );
                }
                catch(Exception ex)
                {
                    Console.WriteLine("Ошибка в получении навигационных ссылок \n"+ ex.Message+"\n"+ex.StackTrace+"\n");
                }
            }    
            return NavigationService;
        }
        public Task<ObservableCollection<NavigationService>> GetPublicNavigationLinks()=>
        Task.FromResult(GetData(NavLinksRequest));
        public async Task<ObservableCollection<NavigationService>> GetPublicNavigationLinksAsync()=>
        await GetDataAsync(NavLinksRequest);

        public Task<ObservableCollection<NavigationService>> GetAuthorizedNavigationLinks(string Login)
        {  
             AuthorizedLinksRequest.Parameters.AddWithValue("@login",Login);
            var result= GetData(AuthorizedLinksRequest);
            AuthorizedLinksRequest.Parameters.Clear();
            return Task.FromResult(result);
        }
        public async Task<ObservableCollection<NavigationService>> GetAuthorizedNavigationLinksAsync(string Login)
        {
            AuthorizedLinksRequest.Parameters.AddWithValue("@login",Login);
            var result= await GetDataAsync(AuthorizedLinksRequest);
            AuthorizedLinksRequest.Parameters.Clear();
            return result;
        }

    }
}