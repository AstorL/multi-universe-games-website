using System;
using System.Data;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace MU_games_website.Data
{
    public class NewsService:MySQLCRService<NewsService>
    {
        private readonly IConfiguration _configuration;
        private MySqlCommand GetNews=new MySqlCommand("SELECT * FROM multi_universe_games.news ORDER BY news_date DESC");
        public int Id {get; set;}
        [Required (ErrorMessage="Введите заголовок.")]
        public string Tittle{get; set;}
        public string Authors {get; set;}
        [Required (ErrorMessage="Введите текст новости, начиная абзацы с табуляции.")]
        public string Text{get; set;}
        public DateTime Date{get; set;}
        public NewsService(IConfiguration configuration):base(configuration)=>_configuration=configuration;
        public NewsService(IConfiguration configuration, int Id,string Tittle, string Text, DateTime Date, string Authors):base(configuration)
        {
            _configuration=configuration;
            this.Id=Id;
            this.Tittle=Tittle;
            this.Text=Text;
            this.Date=Date;
            this.Authors=Authors;
        }
        protected override ObservableCollection<NewsService> GetData(MySqlCommand comm)
        {
            var Result= new ObservableCollection<NewsService>();
            var Data=GetDataTable(comm);
            var AuthorsInfoRequest=new MySqlCommand
            (   
                @"
                SELECT DISTINCT
                    users.login
                FROM
                    multi_universe_games.news_authors,
                    multi_universe_games.users
                WHERE 
                    news_authors.news_id =@NewsId
                    and news_authors.author_id=user_id;
                "
            );
           for(int i=0; i!=Data.Rows.Count; i++)
            {   
                AuthorsInfoRequest.Parameters.AddWithValue("@NewsId",(int)Data.Rows[i][0]);
                var AuthorsInfo= GetDataTable(AuthorsInfoRequest);
                AuthorsInfoRequest.Parameters.Clear();
                string AuthorsBuf="";
                for(int j=0; j!=AuthorsInfo.Rows.Count; j++)
                {
                    AuthorsBuf+=(string)AuthorsInfo.Rows[j][0]+"\t";    
                }
                Result.Add(
                    new NewsService
                    (
                        _configuration,
                        (int)Data.Rows[i][0],
                        (string)Data.Rows[i][1],
                        (string)Data.Rows[i][2],
                        (DateTime)Data.Rows[i][3],
                        AuthorsBuf
                    )
                );
                AuthorsBuf="";
            }
            return Result;
        }

        protected override async Task<ObservableCollection<NewsService>> GetDataAsync(MySqlCommand comm)
        {
            var Result= new ObservableCollection<NewsService>();
            var Data= await GetDataTableAsync(comm); 
            var AuthorsInfoRequest=new MySqlCommand
            (   
                @"
                SELECT DISTINCT
                    users.login
                FROM
                    multi_universe_games.news_authors,
                    multi_universe_games.users
                WHERE 
                    news_authors.news_id =@NewsId
                    and news_authors.author_id=user_id;
                "
            );
           for(int i=0; i!=Data.Rows.Count; i++)
            {   
                 AuthorsInfoRequest.Parameters.AddWithValue("@NewsId",(int)Data.Rows[i][0]);
                var AuthorsInfo= GetDataTable(AuthorsInfoRequest);
                AuthorsInfoRequest.Parameters.Clear();
                string AuthorsBuf="";
                for(int j=0; j!=AuthorsInfo.Rows.Count; j++)
                {
                    AuthorsBuf+=(string)AuthorsInfo.Rows[j][0]+"\t";    
                }
                Result.Add(
                    new NewsService
                    (
                        _configuration,
                        (int)Data.Rows[i][0],
                        (string)Data.Rows[i][1],
                        (string)Data.Rows[i][2],
                        (DateTime)Data.Rows[i][3],
                        AuthorsBuf
                    )
                );
                AuthorsBuf="";
            }
            return Result;
        }
        public Task<ObservableCollection<NewsService>> GetAllNews()=>Task.FromResult(GetData(GetNews));
        public async Task<ObservableCollection<NewsService>> GetAllNewsAsync()=> await GetDataAsync(GetNews);
    }
}    