﻿using System;
using System.Data;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Text;
using MySql.Data.MySqlClient;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Security.Cryptography;

namespace MU_games_website.Data
{

    public class AccountService:MySQLCRService<AccountService>
    {  
        private readonly IConfiguration _configuration;
         private SHA512 Cryptography= SHA512.Create();
         public int Id { get; set;} 
        [Required(ErrorMessage ="Введите имя пользователя")]
        [MaxLength(100,ErrorMessage ="Слишком длинный логин")]
        public string Login { get; set; }
        [Required(ErrorMessage ="Введите пароль")]
        [MinLength(16, ErrorMessage ="Слишком короткий пароль")]
        [MaxLength(255,ErrorMessage ="Слишком длинный пароль" )]
        public string Password { get; set; }
        public DateTime RegistrationDate { get; set;}
        public string User_Role {get; set;}
        private string EnycryptPassword()
        {
            byte[]bytes=Cryptography.ComputeHash(Encoding.UTF8.GetBytes(Login+Password));
            StringBuilder StrB= new StringBuilder();
            for(int i=0; i!=bytes.Length; i++)
            {
                StrB.Append(bytes[i].ToString("x2"));
            }
            return StrB.ToString();
        }
        public AccountService(IConfiguration configuration):base(configuration)=>_configuration = configuration;
        public AccountService(IConfiguration configuration, int id, string login,
        string password, DateTime registrationDate, string user_role):base(configuration)
        {
            _configuration=configuration;
            Id=id;
            Login=login;
            Password=password;
            RegistrationDate=registrationDate;
            User_Role=user_role;
        }
        protected override ObservableCollection<AccountService> GetData(MySqlCommand comm)
        {
            var accounts_table=GetDataTable(comm);
            var accounts= new ObservableCollection<AccountService>();
            for (int i=0; i!=accounts_table.Rows.Count; i++){
                try
                {
                    accounts.Add
                    (
                        new AccountService
                        (
                            _configuration,
                            (int)accounts_table.Rows[i][0],         
                            (string)accounts_table.Rows[i][1],
                            (string)accounts_table.Rows[i][2],
                            (DateTime)accounts_table.Rows[i][3],
                            (string)accounts_table.Rows[i][4]
                        )
                    );
                }
                catch(Exception Ex)
                {
                    Console.WriteLine("Ошибка получения данных учетных записей \n"+Ex.Message+"\n"+Ex.StackTrace+"\n");
                }
            }
            return accounts;
        }
        protected override async Task<ObservableCollection<AccountService>> GetDataAsync(MySqlCommand comm)
        {
            var accounts_table=await GetDataTableAsync(comm);
            var accounts= new ObservableCollection<AccountService>();
            for (int i=0; i!=accounts_table.Rows.Count; i++){
                try
                {
                    accounts.Add
                    (
                        new AccountService
                        (
                            _configuration,
                            (int)accounts_table.Rows[i][0],         
                            (string)accounts_table.Rows[i][1],
                            (string)accounts_table.Rows[i][2],
                            (DateTime)accounts_table.Rows[i][3],
                            (string)accounts_table.Rows[i][4]
                        )
                    );
                }
                catch(Exception Ex)
                {
                    Console.WriteLine("Ошибка получения данных учетных записей \n"+Ex.Message+"\n"+Ex.StackTrace+"\n");
                }
            }
            return accounts;
        }

        public Task<ObservableCollection<AccountService>> GetAllAccounts()=>
        Task.FromResult(GetData(new MySqlCommand("SELECT * FROM multi_universe_games.accounts")));
        public async Task<ObservableCollection<AccountService>> GetAllAccountsAsync()=>
        await GetDataAsync(new MySqlCommand("SELECT * FROM multi_universe_games.accounts"));
        public Task<bool> AuthorizeAccount()
        {
            var authority_request=new MySqlCommand("SELECT * FROM multi_universe_games.accounts WHERE accounts.login=@login AND accounts.password=@password");
            authority_request.Parameters.AddWithValue("@login", Login);
            authority_request.Parameters.AddWithValue("@password", EnycryptPassword());
            try
            {
                if(GetData(authority_request).Count==1)
                {
                    return Task.FromResult(true);
                }
                else
                {
                    return Task.FromResult(false);
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine("Аккаунт по данному логину не найден "+ex.Message+"\n"+ex.StackTrace);
                return Task.FromResult(false);
            }
        }

        public async Task<bool> AuthorizeAccountAsync(){
            var authority_request=new MySqlCommand("SELECT * FROM multi_universe_games.accounts WHERE accounts.login=@login AND accounts.password=@password");
            authority_request.Parameters.AddWithValue("@login", Login);
            authority_request.Parameters.AddWithValue("@password", EnycryptPassword());
            try
            {
                var account=await GetDataAsync(authority_request);
                if(account.Count==1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine("Аккаунт по данному логину не найден "+ex.Message+"\n"+ex.StackTrace);
                return false;
            }
        }

        public Task<AccountService> GetAuthorizedAccount(string login)
        {
            var account_request=new MySqlCommand
            (
                @"
                SELECT
                * 
                FROM 
                multi_universe_games.accounts 
                WHERE accounts.login=@login;
                "
            );
            account_request.Parameters.AddWithValue("@login", login);
            try{
                return Task.FromResult(GetData(account_request)[0]);
            }
            catch(Exception ex){
                Console.WriteLine("Аккаунт по данному логину не найден "+ex.Message+"\n"+ex.StackTrace);
                return null;
            }
        }
        public async Task<AccountService> GetAuthorizedAccountAsync(string login)
        {
            var account_request=new MySqlCommand
            (
                @"
                SELECT
                * 
                FROM 
                multi_universe_games.accounts 
                WHERE accounts.login=@login;
                "
            );
            account_request.Parameters.AddWithValue("@login", login);
            try{
                var account = await GetDataAsync(account_request);
                return account[0];
            }
            catch(Exception ex){
                Console.WriteLine("Аккаунт по данному логину не найден "+ex.Message+"\n"+ex.StackTrace);
                return null;
            }
        }
        
        public void RegisterAccount()
        {
            var registration_request= new MySqlCommand
            (
                @"
                INSERT INTO
                multi_universe_games.users
                (login,password,registration_date)
                VALUES
                (@login,@password,current_date())
                "
            );
            registration_request.Parameters.AddWithValue("@login",Login);
            registration_request.Parameters.AddWithValue("@password",EnycryptPassword());
            RunCommand(registration_request);
        }

        public void RegisterAccountAsync()
        {
            var registration_request= new MySqlCommand
            (
                @"
                INSERT INTO multi_universe_games.users
                (login,password,registration_date)
                VALUES 
                (@login,@password,current_date())
                "    
            );
            registration_request.Parameters.AddWithValue("@login",Login);
            registration_request.Parameters.AddWithValue("@password",EnycryptPassword());
            RunCommandAsync(registration_request);
        }

        public void UpdateAccount()
        {
            var Upd_Com= new MySqlCommand
            (
               @"
                UPDATE multi_universe_games.users
                SET
                password= @password,
                login= @login,
                access_level_id=(SELECT access_level.idaccess_level FROM multi_universe_games.access_level
                WHERE access_level.access_role = @acsessrole),
                WHERE user_id=@usrid;
                "
            );
            Upd_Com.Parameters.AddWithValue("@usrid",Id);
            Upd_Com.Parameters.AddWithValue("@login",Login); 
            Upd_Com.Parameters.AddWithValue("@password",EnycryptPassword());
            Upd_Com.Parameters.AddWithValue("@acsessrole",User_Role); 
            RunCommand(Upd_Com);
        }

        public void UpdateAccountAsync()
        {
            var Upd_Com= new MySqlCommand
            (
               @"
                UPDATE multi_universe_games.users
                SET
                password= @password,
                login= @login,
                access_level_id=(SELECT access_level.idaccess_level FROM multi_universe_games.access_level
                WHERE access_level.access_role = @acsessrole),
                WHERE user_id=@usrid;
                "
            );
            Upd_Com.Parameters.AddWithValue("@usrid",Id);
            Upd_Com.Parameters.AddWithValue("@login",Login); 
            Upd_Com.Parameters.AddWithValue("@password",EnycryptPassword());
            Upd_Com.Parameters.AddWithValue("@acsessrole",User_Role); 
            RunCommandAsync(Upd_Com);
        }

        public void DeleteAccount()
        {
            var DelCom= new MySqlCommand("DELETE FROM multi_universe_games.users WHERE login=@login");
            DelCom.Parameters.AddWithValue("@login",Login);
            RunCommand(DelCom);
        }
        public void DeleteAccountAsync()
        {
            var DelCom= new MySqlCommand("DELETE FROM multi_universe_games.users WHERE login=@login");
            DelCom.Parameters.AddWithValue("@login",Login);
            RunCommandAsync(DelCom);
        }
    }
}
